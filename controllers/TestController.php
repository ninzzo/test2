<?php

namespace app\controllers;

use Yii;
use app\models\Test;
use app\models\TestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TestController implements the CRUD actions for Test model.
 */
class TestController extends Controller
{
    
    /**
     * Lists all Test models.
     * @return mixed
     */
    public function actionIndex()
    {   

        if( Yii::$app->request->post() ){
            $data = Yii::$app->request->post();

            for( $i=1; $i<=100; $i++ ){
                $fields = '';
                for( $j=1; $j<=100; $j++ ){     
                    $val = $data['field'.$j.'_'.$i];

                    if(empty($val)) { 
                        $fields .= '`field'.$j.'`=NULL,'; 
                    } else {
                        $val = (int)$val;
                        if( $val < 1 || $val > 99999 ) { $val = 1; }
                        $fields .= '`field'.$j.'`='.$val.',';
                    }     
                
                }
                $fields = substr($fields, 0, -1);
              
                $sql = "UPDATE `test` SET $fields WHERE `id`=$i;";
                $query = Yii::$app->db->createCommand($sql)->execute();
            }
        } 

        $searchModel = new TestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
    }

}
