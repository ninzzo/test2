<?php

use yii\db\Migration;

/**
 * Class m180531_155427_add_test_data
 */
class m180531_155427_add_test_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {   
        //генерируем 100 значений для 100 строк
        for( $i=1; $i<=100; $i++ ){
            $fields .= '`field'.$i.'`,';
            $val = '(NULL';
            for( $j=1; $j<=100; $j++ ){          
                $val .= ','.rand(1,99999);
            }
            $val .= ')';
            $data .= $val.',';
        }

        $fields = substr($fields, 0, -1);
        $data = substr($data, 0, -1);
        $sql = "INSERT INTO `test` (`id`, ".$fields.") VALUES ".$data;
        $this->execute($sql); 

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Yii::$app->db->createCommand()->truncateTable('test')->execute();
    }

}
