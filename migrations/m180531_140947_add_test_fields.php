<?php

use yii\db\Migration;

/**
 * Class m180531_140947_add_test_fields
 */
class m180531_140947_add_test_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {   
        for( $i=1; $i<=100; $i++ ){
            $field = 'field'.$i;
            $this->addColumn('test', $field, $this->integer());
        }
     

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {   

    }
 
}
