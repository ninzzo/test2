<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Test".
 *
 * @property int $id
 * @property int $field1
 * @property int $field2
 * @property int $field3
 * @property int $field4
 * @property int $field5
 * @property int $field6
 * @property int $field7
 * @property int $field8
 * @property int $field9
 * @property int $field10
 * @property int $field11
 * @property int $field12
 * @property int $field13
 * @property int $field14
 * @property int $field15
 * @property int $field16
 * @property int $field17
 * @property int $field18
 * @property int $field19
 * @property int $field20
 * @property int $field21
 * @property int $field22
 * @property int $field23
 * @property int $field24
 * @property int $field25
 * @property int $field26
 * @property int $field27
 * @property int $field28
 * @property int $field29
 * @property int $field30
 * @property int $field31
 * @property int $field32
 * @property int $field33
 * @property int $field34
 * @property int $field35
 * @property int $field36
 * @property int $field37
 * @property int $field38
 * @property int $field39
 * @property int $field40
 * @property int $field41
 * @property int $field42
 * @property int $field43
 * @property int $field44
 * @property int $field45
 * @property int $field46
 * @property int $field47
 * @property int $field48
 * @property int $field49
 * @property int $field50
 * @property int $field51
 * @property int $field52
 * @property int $field53
 * @property int $field54
 * @property int $field55
 * @property int $field56
 * @property int $field57
 * @property int $field58
 * @property int $field59
 * @property int $field60
 * @property int $field61
 * @property int $field62
 * @property int $field63
 * @property int $field64
 * @property int $field65
 * @property int $field66
 * @property int $field67
 * @property int $field68
 * @property int $field69
 * @property int $field70
 * @property int $field71
 * @property int $field72
 * @property int $field73
 * @property int $field74
 * @property int $field75
 * @property int $field76
 * @property int $field77
 * @property int $field78
 * @property int $field79
 * @property int $field80
 * @property int $field81
 * @property int $field82
 * @property int $field83
 * @property int $field84
 * @property int $field85
 * @property int $field86
 * @property int $field87
 * @property int $field88
 * @property int $field89
 * @property int $field90
 * @property int $field91
 * @property int $field92
 * @property int $field93
 * @property int $field94
 * @property int $field95
 * @property int $field96
 * @property int $field97
 * @property int $field98
 * @property int $field99
 * @property int $field100
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Test';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['field1', 'field2', 'field3', 'field4', 'field5', 'field6', 'field7', 'field8', 'field9', 'field10', 'field11', 'field12', 'field13', 'field14', 'field15', 'field16', 'field17', 'field18', 'field19', 'field20', 'field21', 'field22', 'field23', 'field24', 'field25', 'field26', 'field27', 'field28', 'field29', 'field30', 'field31', 'field32', 'field33', 'field34', 'field35', 'field36', 'field37', 'field38', 'field39', 'field40', 'field41', 'field42', 'field43', 'field44', 'field45', 'field46', 'field47', 'field48', 'field49', 'field50', 'field51', 'field52', 'field53', 'field54', 'field55', 'field56', 'field57', 'field58', 'field59', 'field60', 'field61', 'field62', 'field63', 'field64', 'field65', 'field66', 'field67', 'field68', 'field69', 'field70', 'field71', 'field72', 'field73', 'field74', 'field75', 'field76', 'field77', 'field78', 'field79', 'field80', 'field81', 'field82', 'field83', 'field84', 'field85', 'field86', 'field87', 'field88', 'field89', 'field90', 'field91', 'field92', 'field93', 'field94', 'field95', 'field96', 'field97', 'field98', 'field99', 'field100'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'field1' => 'Field1',
            'field2' => 'Field2',
            'field3' => 'Field3',
            'field4' => 'Field4',
            'field5' => 'Field5',
            'field6' => 'Field6',
            'field7' => 'Field7',
            'field8' => 'Field8',
            'field9' => 'Field9',
            'field10' => 'Field10',
            'field11' => 'Field11',
            'field12' => 'Field12',
            'field13' => 'Field13',
            'field14' => 'Field14',
            'field15' => 'Field15',
            'field16' => 'Field16',
            'field17' => 'Field17',
            'field18' => 'Field18',
            'field19' => 'Field19',
            'field20' => 'Field20',
            'field21' => 'Field21',
            'field22' => 'Field22',
            'field23' => 'Field23',
            'field24' => 'Field24',
            'field25' => 'Field25',
            'field26' => 'Field26',
            'field27' => 'Field27',
            'field28' => 'Field28',
            'field29' => 'Field29',
            'field30' => 'Field30',
            'field31' => 'Field31',
            'field32' => 'Field32',
            'field33' => 'Field33',
            'field34' => 'Field34',
            'field35' => 'Field35',
            'field36' => 'Field36',
            'field37' => 'Field37',
            'field38' => 'Field38',
            'field39' => 'Field39',
            'field40' => 'Field40',
            'field41' => 'Field41',
            'field42' => 'Field42',
            'field43' => 'Field43',
            'field44' => 'Field44',
            'field45' => 'Field45',
            'field46' => 'Field46',
            'field47' => 'Field47',
            'field48' => 'Field48',
            'field49' => 'Field49',
            'field50' => 'Field50',
            'field51' => 'Field51',
            'field52' => 'Field52',
            'field53' => 'Field53',
            'field54' => 'Field54',
            'field55' => 'Field55',
            'field56' => 'Field56',
            'field57' => 'Field57',
            'field58' => 'Field58',
            'field59' => 'Field59',
            'field60' => 'Field60',
            'field61' => 'Field61',
            'field62' => 'Field62',
            'field63' => 'Field63',
            'field64' => 'Field64',
            'field65' => 'Field65',
            'field66' => 'Field66',
            'field67' => 'Field67',
            'field68' => 'Field68',
            'field69' => 'Field69',
            'field70' => 'Field70',
            'field71' => 'Field71',
            'field72' => 'Field72',
            'field73' => 'Field73',
            'field74' => 'Field74',
            'field75' => 'Field75',
            'field76' => 'Field76',
            'field77' => 'Field77',
            'field78' => 'Field78',
            'field79' => 'Field79',
            'field80' => 'Field80',
            'field81' => 'Field81',
            'field82' => 'Field82',
            'field83' => 'Field83',
            'field84' => 'Field84',
            'field85' => 'Field85',
            'field86' => 'Field86',
            'field87' => 'Field87',
            'field88' => 'Field88',
            'field89' => 'Field89',
            'field90' => 'Field90',
            'field91' => 'Field91',
            'field92' => 'Field92',
            'field93' => 'Field93',
            'field94' => 'Field94',
            'field95' => 'Field95',
            'field96' => 'Field96',
            'field97' => 'Field97',
            'field98' => 'Field98',
            'field99' => 'Field99',
            'field100' => 'Field100',
        ];
    }
}
