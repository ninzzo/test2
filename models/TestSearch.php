<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Test;

/**
 * TestSearch represents the model behind the search form of `app\models\Test`.
 */
class TestSearch extends Test
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'field1', 'field2', 'field3', 'field4', 'field5', 'field6', 'field7', 'field8', 'field9', 'field10', 'field11', 'field12', 'field13', 'field14', 'field15', 'field16', 'field17', 'field18', 'field19', 'field20', 'field21', 'field22', 'field23', 'field24', 'field25', 'field26', 'field27', 'field28', 'field29', 'field30', 'field31', 'field32', 'field33', 'field34', 'field35', 'field36', 'field37', 'field38', 'field39', 'field40', 'field41', 'field42', 'field43', 'field44', 'field45', 'field46', 'field47', 'field48', 'field49', 'field50', 'field51', 'field52', 'field53', 'field54', 'field55', 'field56', 'field57', 'field58', 'field59', 'field60', 'field61', 'field62', 'field63', 'field64', 'field65', 'field66', 'field67', 'field68', 'field69', 'field70', 'field71', 'field72', 'field73', 'field74', 'field75', 'field76', 'field77', 'field78', 'field79', 'field80', 'field81', 'field82', 'field83', 'field84', 'field85', 'field86', 'field87', 'field88', 'field89', 'field90', 'field91', 'field92', 'field93', 'field94', 'field95', 'field96', 'field97', 'field98', 'field99', 'field100'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Test::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' =>false,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'field1' => $this->field1,
            'field2' => $this->field2,
            'field3' => $this->field3,
            'field4' => $this->field4,
            'field5' => $this->field5,
            'field6' => $this->field6,
            'field7' => $this->field7,
            'field8' => $this->field8,
            'field9' => $this->field9,
            'field10' => $this->field10,
            'field11' => $this->field11,
            'field12' => $this->field12,
            'field13' => $this->field13,
            'field14' => $this->field14,
            'field15' => $this->field15,
            'field16' => $this->field16,
            'field17' => $this->field17,
            'field18' => $this->field18,
            'field19' => $this->field19,
            'field20' => $this->field20,
            'field21' => $this->field21,
            'field22' => $this->field22,
            'field23' => $this->field23,
            'field24' => $this->field24,
            'field25' => $this->field25,
            'field26' => $this->field26,
            'field27' => $this->field27,
            'field28' => $this->field28,
            'field29' => $this->field29,
            'field30' => $this->field30,
            'field31' => $this->field31,
            'field32' => $this->field32,
            'field33' => $this->field33,
            'field34' => $this->field34,
            'field35' => $this->field35,
            'field36' => $this->field36,
            'field37' => $this->field37,
            'field38' => $this->field38,
            'field39' => $this->field39,
            'field40' => $this->field40,
            'field41' => $this->field41,
            'field42' => $this->field42,
            'field43' => $this->field43,
            'field44' => $this->field44,
            'field45' => $this->field45,
            'field46' => $this->field46,
            'field47' => $this->field47,
            'field48' => $this->field48,
            'field49' => $this->field49,
            'field50' => $this->field50,
            'field51' => $this->field51,
            'field52' => $this->field52,
            'field53' => $this->field53,
            'field54' => $this->field54,
            'field55' => $this->field55,
            'field56' => $this->field56,
            'field57' => $this->field57,
            'field58' => $this->field58,
            'field59' => $this->field59,
            'field60' => $this->field60,
            'field61' => $this->field61,
            'field62' => $this->field62,
            'field63' => $this->field63,
            'field64' => $this->field64,
            'field65' => $this->field65,
            'field66' => $this->field66,
            'field67' => $this->field67,
            'field68' => $this->field68,
            'field69' => $this->field69,
            'field70' => $this->field70,
            'field71' => $this->field71,
            'field72' => $this->field72,
            'field73' => $this->field73,
            'field74' => $this->field74,
            'field75' => $this->field75,
            'field76' => $this->field76,
            'field77' => $this->field77,
            'field78' => $this->field78,
            'field79' => $this->field79,
            'field80' => $this->field80,
            'field81' => $this->field81,
            'field82' => $this->field82,
            'field83' => $this->field83,
            'field84' => $this->field84,
            'field85' => $this->field85,
            'field86' => $this->field86,
            'field87' => $this->field87,
            'field88' => $this->field88,
            'field89' => $this->field89,
            'field90' => $this->field90,
            'field91' => $this->field91,
            'field92' => $this->field92,
            'field93' => $this->field93,
            'field94' => $this->field94,
            'field95' => $this->field95,
            'field96' => $this->field96,
            'field97' => $this->field97,
            'field98' => $this->field98,
            'field99' => $this->field99,
            'field100' => $this->field100,
        ]);

        return $dataProvider;
    }
}
