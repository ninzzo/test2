<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-index">

<?php
$form = ActiveForm::begin([
        'id' => 'pw-form',
    ]);

$columns = [];
$GLOBALS["n"] = 0;
for( $i=1; $i<=100; $i++ ){
      $field = 'field'.$i;
      if($i == 1){ array_push($columns,['class' => 'yii\grid\SerialColumn']);}
      array_push($columns, [
            'attribute' => $field,
            'value' => function($model, $key, $index, $column){
                  //callback функция не видит $i. Или строим костыль через global или выводим все 100 полей вручную.                   
                  $GLOBALS["n"] =  $GLOBALS["n"]+1;
                  if($GLOBALS["n"] == 101) {$GLOBALS["n"]=1;}
                  return Html::textInput('field'.$GLOBALS["n"].'_'.$key, $model['field'.$GLOBALS["n"]], [ 'type' => 'number', 'min'=> 1, 'max'=> 99999]);
            },
            'format' => 'raw'
            ]
      );
}

?>

<?= GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => false,
      'rowOptions' => function ($model, $index, $widget, $grid) {
      return ['style' => 'font-size:8px'];
      },
      'columns' => $columns,
]); 
echo Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'pw-button']);
ActiveForm::end();
?>
</div>


