
### Тестовое задание номер2. Автор: Наянзин Сергей

"Есть таблица 100 x 100, в каждой клетке - текстовое поле.
Каждое значение - натуральное число от 1 до 99999
Реализовать используя фреймворк Yii возможность произвольно вводить/изменять числа в полях. Т.е. если добавить/изменить/удалить числа, они должны сохраниться при нажатии кнопки Update внизу таблицы.

Таблицу создать миграцией. Внешний вид - на свое усмотрение, считать что с этим будут работать люди."

```
1. Уснановил YII2 через composer
2. Создал миграции: 
-Создание таблицы с field1 - field10;
-Генерация 10000 записей для таблицы.
3. Создал CRUD для таблицы "Test"
4. view для отображения таблицы 100на100 вот здесь - /views/test/index.php
controller для обработки - /controllers/TestConstroller.php 


```
Проверить в работе:
http://ninzzo-test.cx.ua/web/index.php?r=test%2Findex
Этот сервер может принять только 1000 запросов за раз. Поэтому все id > 10 не обрабатываются. Не могу изменить php_value max_input_vars 1000 в настройках Apache, нет доступа. 

п.с. Я бы сделал что бы каждое поле редактировалось через ajax и сохранялось сразу при изменении + валидации на месте, но задание требует нажимать кнопку Update и отправлять 10000 переменных POST. Очень странное задание.
